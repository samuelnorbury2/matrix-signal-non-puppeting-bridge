#!/usr/bin/env nix-shell
#!nix-shell -i bash shell.nix

if (! pgrep -f 'signald-supervisor.conf'); then
    supervisord -c signald-supervisor.conf
fi

while (! tail -n1 logs/signald.log | grep 'Started signald'); do
    sleep 5
    echo 'Waiting for signald to start'
done

supervisord -c bridge-supervisor.conf
