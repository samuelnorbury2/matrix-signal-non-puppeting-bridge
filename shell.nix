let
  sources = import ./nix/sources.nix;
  pkgs = import sources.nixpkgs {};
in
pkgs.mkShell {
  buildInputs = [
    pkgs.bash
    pkgs.python310
    pkgs.pipenv
    pkgs.python3Packages.supervisor
    pkgs.python310Packages.python-olm
    pkgs.olm
    pkgs.signald
  ];
}
