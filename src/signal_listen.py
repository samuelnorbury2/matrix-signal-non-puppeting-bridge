import asyncio
import argparse
import logging
from bridge import Bridge

parser = argparse.ArgumentParser()
parser.add_argument(
    "-log",
    "--loglevel",
    default="warning",
    help="Provide logging level. Example --loglevel debug, default=warning",
)

args = parser.parse_args()
logging.basicConfig(level=args.loglevel.upper())


async def main():
    logging.info("Creating Bridge class for signal.")
    bridge = Bridge()
    await bridge.initialize_matrix()
    await bridge.main_signal()


asyncio.run(main())
